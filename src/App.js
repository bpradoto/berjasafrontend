import React from "react";
import { withStyles, Grid } from "@material-ui/core";
import AboutUsContentStyle from "asset/css/AboutUsContent.jsx";
import GridContainer from "../../css/material-ui-react/Grid/GridContainer.jsx"
import GridItem from "../../css/material-ui-react/Grid/GridItem.jsx"

class AboutUsContent extends React.Component{
    render(){
      const {classes}= this.props
      return (
        <div className={classes.sections}>
        <div className={classes.container}>
        <div className={classes.title}>
        <div 
        style={{    
          marginTop: "40px",
          marginBottom: "100px",
          fontFamily: "'Roboto', sans-serif"}}>
            <h2 className={classes.titles}>Tentang Kami</h2>
            <p style= {{ 
              fontWeight: '100',
              fontFamily: "'Roboto', sans-serif"}}>
            Cendikiawan Nusantara Ilhami merupakan perusahaan yang bergerak dalam bidang pengembangan diri, 
            membantu pribadi memahami potensi dan memaksimalkan potensi 
            tersebut sehingga menjadi individu yang memiliki kompetensi dalam mengaktualisasi diri.
            </p>
            <br/>
            <hr/>
        </div>
            <div 
            style={{
              marginTop: "100px",
              marginBottom: "100px",
              fontFamily: "'Roboto', sans-serif"}}>
            <Grid>
              <h1 className={classes.asd}>
                Visi
              </h1>
              <p style= {{
                fontWeight: '100',
                fontFamily: "'Roboto', sans-serif",}}>
              Terwujudnya Generasi Penerus Bangsa Indonesia yang memiliki 
              KEBERMAKNAAN HIDUP, RELIGIUS, dan  CINTA TANAH AIR
              </p>
            </Grid>
            </div>
            <div 
            style={{
              marginTop: "100px",
              marginBottom: "100px",
              fontFamily: "'Roboto', sans-serif"}}>
            <Grid>
              <h1 className={classes.Spacing}
                  style={{fontFamily: "'Roboto', sans-serif"}}>
                Misi
              </h1>
              <ul className={classes.listSytle}
              style= {{textAlign: "left",
              fontFamily: "'Roboto', sans-serif",
              marginLeft: "100px"}}>
                <li style= {{
                  fontWeight: '100',
                  fontFamily: "'Roboto', sans-serif"}}
                className={classes.listitem}>
                  Menjadikan Cendikiawan nusantara ilhami sebagai   
                  perusahaan yang kompeten dan memiliki kreadibilitas tinggi
                </li>
                <li style= {{
                  fontWeight: '100',
                  fontFamily: "'Roboto', sans-serif"}}
                className={classes.listitem}>
                Melaksanakan pengujian Test Psikologi dengan prinsip ketepatan hasil test 
                dan Jaminan kerahasiaan hasil test.
                </li>
                <li style= {{
                  fontWeight: '100',
                  fontFamily: "'Roboto', sans-serif"}}
                className={classes.listitem}>
                Melakukan transformasi perbaikan karakter generasi penerus bangsa melalui 
                The Success Formula 6 in 1 (TSF 61).
                </li>
                <li style= {{
                  fontWeight: '100',
                  fontFamily: "'Roboto', sans-serif"}}
                className={classes.listitem}>
                Melakukan pelatihan Kerjasama, integritas, kebermaknaan hidup, cinta tanah air, dan religius.
                </li>
                <li style= {{
                  fontWeight: '100',
                  fontFamily: "'Roboto', sans-serif"}}
                className={classes.listitem}>
                Melakukan percepatan transfomasi budaya enterpreneur menggunakan TSF 61
                </li>
              </ul>
            </Grid>
          <iframe width="560" height="315" src="https://www.youtube.com/embed/h07lA_q48vg"
           frameborder="0" style={{marginTop: '35px'}} allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
           allowfullscreen alt="video">
          </iframe>
            </div>
          </div>
        </div>
        </div>
        )
    }
}

export default withStyles(AboutUsContentStyle)(AboutUsContent);