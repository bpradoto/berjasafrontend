import {container,title} from "./material-kit-react.jsx";

const AboutUsContentStyle = {
    sections: {
      padding: "18px 0 ",
      fontFamily: 'Helvetica',
    },
    container,
    title: {
      ...title,
      marginTop: "30px",
      minHeight: "32px",
      textDecoration: "none",
      textAlign: "center",
    },
    space50: {
      height: "50px",
      display: "block"
    },
    space70: {
      height: "70px",
      display: "block"
    },
    icons: {
      width: "17px",
      height: "17px",
      color: "#FFFFFF"
    },
    main: {
      background: "#FFFFFF",
      position: "relative",
      zIndex: "3"
    },
    mainRaised: {
      margin: "-60px 30px 0px",
      borderRadius: "6px",
      boxShadow:
        "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    listSytle: {
      listStyle:"decimal",
    },
    listitem:{
      marginBottom:"10px",
    },
    Spacing:{
    color: "#ffb935",
    fontSize: "80px",
    fontWeight: "100",
    },
    titles:{
      fontFamily: 'Helvetica',
    },
    asd:{
      color: "#369dff",
      fontSize: "80px",
      fontWeight:"100",
    }

}
export default AboutUsContentStyle;
  